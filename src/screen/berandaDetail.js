import React from "react";
import { AppRegistry, View, StyleSheet, StatusBar, Linking, AsyncStorage } from "react-native";
import {
    Button,
    Text,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Left,
    Badge,
    Right,
    Icon,
    Title,
    Input,
    InputGroup,
    Item,
    Tab,
    Tabs,
    Footer,
    FooterTab,
    Label,
    Thumbnail,
    ListItem,
    CheckBox,
    Picker,
    TouchableOpacity
} from "native-base";
import Image from 'react-native-image-progress';
import ProgressCircle from 'react-native-progress/Circle';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';


export default class BerandaDetail extends React.Component {



    constructor(props) {
        super(props);
        this.state = {
            nomor_laporan: this.props.navigation.state.params.nomor_laporan,
            dataLaporanDetail: [],
            selected: "key0",
            status: "",
            proses: "",
            coba: "coba",
            dataUser: [],
            userId: ""
        };
    }

    componentDidMount() {

        fetch("http://ec2-13-250-62-76.ap-southeast-1.compute.amazonaws.com:7000/api/v1/laporan/findById/" + this.state.nomor_laporan, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then((data) => {
                console.log(data.response);
                this.setState({
                    dataLaporanDetail: data.response[0]

                });
                console.log(this.state.dataLaporanDetail)
            })

        AsyncStorage.getItem("userid", (error, result) => {
            if (result) {
                console.log("userid : " + result)
                this.state.userId = result
            }
            this.state.userId = result;
            fetch("http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/profile/" + result, {
                method: "GET",
                headers: {
                    'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
                }
            })
                .then((response) => response.json())
                .then((data) => {

                    this.setState({
                        dataUser: data.data,
                    });

                    console.log(this.state.dataUser)
                })
                .catch((error) => {
                    console.log(error);
                })
        })

    }


    ShowHideTextComponentView = () => {

        if (this.state.status == "key1") {
            this.setState({ status: false })
        }
        else {
            this.setState({ status: true })
        }
    }

    onValueChange3(value) {
        this.setState({
            selected: value
        });
        if (value == "key0") {
            this.setState({ status: false })

        }
        else {
            this.setState({ status: true })

            return fetch("http://ec2-13-250-62-76.ap-southeast-1.compute.amazonaws.com:7000/api/v1/laporan/updateStatus/" + this.state.dataLaporanDetail.nomor_laporan, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
                },
                body: JSON.stringify({

                    status: "dalam pengerjaan",
                    updater: this.state.dataUser.name

                })
            })
                .then(response => response.json())
                .then((data) => {
                    console.log("beranda updater : "+this.state.dataUser.name);
                    this.setState({
                        coba: data.response

                    });
                    console.log(this.state.coba)
                    this.state.dataLaporanDetail.status = "Dalam Pengerjaan";
                    this.state.dataLaporanDetail.icon = "#f8e71c";
                    this.setState({ refreshing: true });
                })
        }
    }



    render() {
        const {goBack} = this.props.navigation;
        return (
            <Container style={{ marginTop: 25, backgroundColor: "#fff" }}>
                <Header style={{ backgroundColor: "#fff" }}>
                    <Left>
                        <Icon name="arrow-back" onPress={() => goBack(null)} style={{ marginLeft: "5%" }} />

                    </Left>
                    <Body style={{ backgroundColor: "#fff", alignItems: "center" }}>
                        <Title style={{ color: "#000" }}>Detail Laporan</Title>
                    </Body>
                    <Right />
                </Header>
                <Content>
                <View >

                        <Image style={{ height: 200, width: null, fles:1 }}
                            source={{ uri: this.state.dataLaporanDetail.images }}
                            indicator={ProgressCircle} />

                    </View>
                    <Card>



                        <CardItem>
                            <Left>
                                <Thumbnail small source={{ uri: this.state.dataLaporanDetail.avatar_pelapor }} />
                                <Body>
                                    <Text>{this.state.dataLaporanDetail.pelapor}</Text>
                                    <Text style={{ fontSize: responsiveFontSize(1.5) }}>No Tiket : {this.state.dataLaporanDetail.nomor_laporan}</Text>
                                </Body>
                            </Left>
                            <Right>
                                <Text style={{ fontStyle: "italic", fontSize: responsiveFontSize(1.5) }}> {this.state.dataLaporanDetail.tanggal_laporan} </Text>
                            </Right>
                        </CardItem>

                        <CardItem>
                            <Text style={{ fontSize: 12 }}>{this.state.dataLaporanDetail.description} </Text>
                        </CardItem>

                        <CardItem style={{ height: "25%" }}>

                            <View style={{ flex: 1, backgroundColor: "#fff", width: "100%", height: "100%" }}>
                                {/*<View style={{ backgroundColor: "#000", height: "40%", width: "100%" }}>
                                </View>*/}
                                <View style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between'
                                }}>
                                    <View style={{ marginTop: "-2%", alignItems: 'center', flexDirection: 'row', width: "35%", height: "100%" }} >
                                        <View style={{ alignItems: 'center', marginLeft: "10%", justifyContent: 'center', flexDirection: 'row', width: "20%", height: "50%" }}>
                                            <Image style={{}} source={require("../../img/asset/ic-map.png")} />
                                        </View>
                                        <Text style={{ marginTop: "-2%", alignItems: 'center', marginLeft: "10%", fontSize: 10 }}>{this.state.dataLaporanDetail.lokasi}</Text>
                                    </View>

                                    <View style={{ marginTop: "-2%", alignItems: 'center', flexDirection: 'row', width: "35%", height: "100%" }} >
                                        <View style={{ borderRadius: 100, backgroundColor: this.state.dataLaporanDetail.icon, alignItems: 'center', marginLeft: "10%", justifyContent: 'center', flexDirection: 'row', width: "10%", height: "30%" }}>
                                            {/*<Image style={{ borderRadius: 100 }} source={require("../../img/asset/ic-menunggu.png")} />*/}
                                        </View>
                                        <Text style={{ marginTop: "-2%", alignItems: 'center', marginLeft: "5%", fontSize: 10 }}>{this.state.dataLaporanDetail.status}</Text>

                                    </View>
                                </View>
                            </View>
                            {/*<View transparent style={{ flex: 1, height: "100%", flexDirection: 'row', width: "70%", left: "1%" }}>
                                <Image style={{}}
                                    source={require("../../img/asset/ic-map.png")} />
                                <Text note style={{ fontSize: responsiveFontSize(1.5), width: "70%", marginLeft: "5%", marginTop: "-2%" }}>{this.state.dataLaporanDetail.lokasi}</Text>
                            </View>

                            <View transparent style={{ flex: 1, flexDirection: 'row', height: "100%", width: "30%", right: "1%", paddingTop: "5%" }}>
                                <Image style={{ height: "60%", width: "6%" }}
                                    source={require("../../img/asset/ic-menunggu.png")}
                                />
                                <Text note style={{ fontSize: responsiveFontSize(1.5), width: "70%", marginLeft: "5%", marginTop: "0%" }}>{this.state.dataLaporanDetail.status}</Text>
                            </View>*/}
                        </CardItem>

                        <ListItem style={{ height: "15%" }}>
                            <Left>
                                <Text style={{ fontSize: 12 }}>Kategori</Text>
                            </Left>

                            <Right>
                                <Text style={{ fontSize: 12 }}>{this.state.dataLaporanDetail.kategori}</Text>
                            </Right>
                        </ListItem>
                        <ListItem style={{ height: "15%" }}>
                            <Left>
                                <Text style={{ fontSize: 12, width: 60 }}>Tag BUMN</Text>
                            </Left>


                            <Label style={{ textAlign: 'center', fontSize: responsiveFontSize(1.5), color: "black", backgroundColor: '#e8f0ff', marginRight: "1%", borderRadius: 2 }}>
                                {this.state.dataLaporanDetail.nama_bumn}
                            </Label>

                        </ListItem>


                    </Card>



                    <Card >
                        <Picker style={{
                            flex: 1, flexDirection: 'row', marginLeft: "10%", width: "80%", height: 45, borderRadius: 4,
                            borderWidth: 0.5,


                            borderColor: "#f0f0f0"
                        }}
                            iosHeader="Select one"
                            mode="dropdown"
                            selectedValue={this.state.selected}
                            onValueChange={this.onValueChange3.bind(this)}
                        >
                            <Item label="Menunggu Solusi" value="key0"></Item>
                            <Item label="Dalam Pengerjaan" value="key1" ></Item>

                        </Picker>
                    </Card>


                    <Button bordered light style={{ borderRadius: 5, paddingLeft: "5%", flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "#0052a8", marginTop: "3%", marginRight: "10%", marginLeft: "10%", width: responsiveWidth(80) }}
                        onPress={this.onClickTeruskanKirim.bind()}>
                        <Image source={require("../../img/asset/ic-send-email.png")}
                        />
                        <Text style={{}}>Teruskan ke Unit Terkait</Text>
                    </Button>

                    {

                        this.state.status ?

                            <View>

                                <Button bordered light style={{ borderRadius: 5, paddingLeft: "5%", flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "#0052a8", marginTop: "3%", marginRight: "10%", marginLeft: "10%", width: responsiveWidth(80) }}
                                    onPress={() => this.props.navigation.navigate("Tambaheviden", { nomor_laporan: this.state.dataLaporanDetail.nomor_laporan })}
                                >
                                    <Left>
                                        <Image source={require("../../img/asset/camera.png")}
                                        />
                                    </Left>

                                    <Text style={{ textAlign: "center" }}>Bukti Solusi</Text>



                                </Button>


                                <Button bordered light style={{ borderRadius: 5, paddingLeft: "5%", flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "#0052a8", marginTop: "3%", marginRight: "10%", marginLeft: "10%", width: responsiveWidth(80) }}
                                    onPress={() => this.props.navigation.navigate("ReturnLaporan", { nomor_laporan: this.state.dataLaporanDetail.nomor_laporan })}>
                                    <Left style={{}}>
                                        <Image style={{}} source={require("../../img/asset/ic-back.png")} />
                                    </Left>


                                    <Text style={{}}>Return Laporan</Text>

                                </Button>
                            </View> : null
                    }

                    <Text style={{ marginBottom: "5%", fontSize: responsiveFontSize(1.5), width: "90%", textAlign: "center", marginTop: "5%", marginLeft: "5%" }}>*Anda bisa bagikan pelaporan ini ke tim untuk disolusikan atau dapat mengubah status menjadi on progress apabila pekerjaan ini sedang berlangsung</Text>



                </Content>

            </Container>

        );
    }


    onClickTeruskanKirim = () => {
        return fetch("http://ec2-13-250-62-76.ap-southeast-1.compute.amazonaws.com:7000/api/v1/laporan/updateStatus/" + this.state.dataLaporanDetail.nomor_laporan, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
            },
            body: JSON.stringify({

                status: "dalam pengerjaan",
                updater: this.state.dataUser.name

            })
        })
            .then(response => response.json())
            .then((data) => {

                fetch("http://ec2-13-250-62-76.ap-southeast-1.compute.amazonaws.com:7000/api/v1/share/sms", {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
                    },
                    body: JSON.stringify({

                        nomor_laporan: this.state.dataLaporanDetail.nomor_laporan,
                        idPetugas: this.state.userId,
                        namaPetugas: this.state.dataUser.name,
                        fotoPetugas: this.state.dataUser.profilePicture

                    })
                })
                    .then(response => response.json())
                    .then((data) => {
                        console.log(data.response);
                        this.setState({
                            coba: data.response

                        });
                        console.log(this.state.coba)
                        this.state.dataLaporanDetail.status = "Dalam Pengerjaan";
                        this.state.dataLaporanDetail.icon = "#f8e71c";
                        this.setState({ refreshing: true });
                        Linking.openURL('sms:/open?addresses=&body=' + data.response);

                    })
            })

    }







}

const styles = StyleSheet.create({
    imgFooter: {
        height: "40%",
        width: "22%",
        marginBottom: "5%"
    },
    footer: {
        backgroundColor: "#fff"
    },
    fontFooter: {
        color: "#000",
        fontSize: responsiveFontSize(1.5)
    },
    oneButton: {
        height: 60,
        backgroundColor: '#e41c23',
    },

    oneButtonView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 20,
        justifyContent: 'center',
    },

    payTextDollar: {
        flex: 1,
        color: '#ffffff',
        fontSize: 20,
        fontFamily: 'MetaOffc-Light',
        textAlign: 'center',
    },

    imageButton: {
        marginTop: 6,
    },
    flexDetailPengerjaan: {
        textAlign: 'center', fontSize: responsiveFontSize(1.5), color: "black", marginLeft: "15%", marginRight: "40%", borderRadius: 2
    }
});

