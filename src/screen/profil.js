import React from "react";
import { StatusBar, Image, MapView, AsyncStorage, Alert, TextInput, ImageBackground, View, StyleSheet, BackHandler } from "react-native";
import {
    Button,
    Text,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Footer,
    FooterTab,
    H1,
    Thumbnail,
    Form,
    Item,
    Label,
    Input,
    List,
    ListItem

} from "native-base";

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';





export default class Profil extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            dataUser: [],
            userId: ""
        }
        BackHandler.addEventListener('hardwareBackPress',this.handleBackButton);
    }

    componentDidMount() {
        AsyncStorage.getItem("userid", (error, result) => {
            if (result) {
                console.log("userid : " + result)
            }
            this.state.userId = result;
            fetch("http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/profile/" + result, {
                method: "GET",
                headers: {
                    'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
                }
            })
                .then((response) => response.json())
                .then((data) => {

                    this.setState({
                        dataUser: data.data,
                    });
                    // AsyncStorage.setItem("name",data.data.name);
                    // AsyncStorage.setItem("email",data.data.email);
                    // AsyncStorage.setItem("nik",data.data.nik);
                    // AsyncStorage.setItem("bumnName",data.data.bumnName);
                    // AsyncStorage.setItem("mobilePhone",data.data.mobilePhone);
                    // AsyncStorage.setItem("profilePicture",data.data.profilePicture);

                    console.log(this.state.dataUser)
                })
                .catch((error) => {
                    console.log(error);
                })
        })
    }

    render() {
        return (
            <Container style={{
                marginTop: 25,
                backgroundColor: "white"
            }}>
                <Header style={{ alignItems: "center", backgroundColor: "white" }}>


                    <Title style={{ color: "black" }}>Profil</Title>

                </Header>
                <Content >
                    <View style={{ alignContent: "center", alignItems: "center", marginTop: "5%" }}>
                        <Thumbnail large source={{ uri: this.state.dataUser.profilePicture }} style={{}} />
                        <Text style={{ fontSize: responsiveFontSize(2.5), fontWeight: "bold", marginTop: "5%" }}>{this.state.dataUser.name}</Text>
                        <Text style={{ fontSize: responsiveFontSize(2), marginTop: "1%" }}>{this.state.dataUser.mobilePhone}</Text>
                        <Text style={{ fontSize: responsiveFontSize(2), }}>{this.state.dataUser.email}</Text>
                        <Text style={{ fontSize: responsiveFontSize(2), }}>{this.state.dataUser.nik}</Text>
                        <Label style={{ textAlign: 'center', fontSize: responsiveFontSize(2), color: "#3f84f4", marginTop: "4%", backgroundColor: '#e8f0ff', borderRadius: 2, width: "50%" }}>
                            {this.state.dataUser.bumnName}
                        </Label>
                    </View>
                    <List style={{ marginRight: "5%" }}>
                        <ListItem>
                        </ListItem>
                        <ListItem onPress={() => this.props.navigation.navigate("UbahProfil")}>
                            <Body>
                                <Text>Ubah Profil</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>
                        <ListItem onPress={() => this.props.navigation.navigate("UbahPassword")}>
                            <Body>
                                <Text>Ubah Password</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>
                    </List>
                    <Button onPress={() => {



                        AsyncStorage.getItem("token", (error, result) => {
                            if (result) {
                                console.log("token : " + result)
                            }

                            fetch("http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/logout", {
                                method: "POST",
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json',
                                    'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
                                },
                                body: JSON.stringify({
                                    token: result,
                                })
                            })
                                .then((response) => response.json())
                                .then((data) => {

                                    alert("Logout berhasil!");
                                    AsyncStorage.removeItem("token");
                                    AsyncStorage.removeItem("userId");
                                    AsyncStorage.removeItem("bumnId");
                                    this.props.navigation.navigate("BeforeLogin")
                                    console.log(this.state.dataUser)
                                })
                                .catch((error) => {
                                    alert("Logout gagal!");
                                })
                        })

                    }} style={{ borderRadius: 5, backgroundColor: "#0052A8", marginLeft: "10%", width: "80%", marginTop: "5%", flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text>Log Out</Text>
                    </Button>
                </Content>
                <Footer>
                    <FooterTab style={styles.footer}>
                        <Button
                            onPress={() => this.props.navigation.navigate("Beranda")}
                        >

                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/navbar/ico-tabs-home-1.png")}
                            />

                            <Text uppercase={false} style={styles.fontFooter}>Beranda</Text>
                        </Button>
                        <Button
                            onPress={() => this.props.navigation.navigate("Riwayat")}
                        >
                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/navbar/ico-tabs-riwayat-1.png")}
                            />
                            <Text uppercase={false} style={styles.fontFooter}>Riwayat</Text>
                        </Button>
                        <Button
                            onPress={() => this.props.navigation.navigate("Bantuan")}

                            vertical
                        >
                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/navbar/ico-tabs-help-1.png")}
                            />
                            <Text uppercase={false} style={styles.fontFooter}>Bantuan</Text>
                        </Button>
                        <Button
                        >
                            <Thumbnail square
                                style={styles.imgFooter}
                                source={require("../../img/asset/navbar/ico-tabs-profile.png")}
                            />
                            <Text uppercase={false} style={styles.fontFooter}>Akun</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }

      handleBackButton() {
        return true;
    }

    static route = {
styles: {
gestures: null,
}
}

}

const styles = StyleSheet.create({
    imgFooter: {
        height: "40%",
        width: "22%",
        marginBottom: "5%"
    },
    footer: {
        backgroundColor: "#fff"
    },
    fontFooter: {
        color: "#000",
        fontSize: responsiveFontSize(1.5)
    }
})





