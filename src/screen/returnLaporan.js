import React, { Component } from 'react';
import {
    Button,
    Text,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Left,
    Badge,
    Right,
    Icon,
    Title,
    Input,
    InputGroup,
    Item,
    Tab,
    Tabs,
    Footer,
    FooterTab,
    Label,
    Thumbnail,
    ListItem,
    Textarea,
    CheckBox,
    Picker,
    TouchableOpacity
} from "native-base";
import { AppRegistry, View, StyleSheet, Alert, StatusBar, Image } from "react-native";
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

export default class ReturnLaporan extends Component {
    constructor(props) {
        super(props)
        this.state = {
            nomor_laporan: this.props.navigation.state.params.nomor_laporan,
            komentar: ""
        }
    }

    render() {
        return (
            <Container style={{ marginTop: 25 }}>
                <Header style={{ backgroundColor: "#fff" }}>
                    <Left>
            
                            <Icon name="arrow-back"  onPress={() => this.props.navigation.navigate("BerandaDetail", { nomor_laporan: this.state.nomor_laporan })} />
                       
                    </Left>
                    <Body style={{ backgroundColor: "#fff", alignItems: "center" }}>
                        <Title style={{ color: "#000" }}>Return Laporan</Title>
                    </Body>
                    <Right />
                </Header>
                <Content>
                    <Card style={{ backgroundColor: 'powderblue' }}>
                        <CardItem style={{ backgroundColor: 'powderblue' }}>
                            <Left>
                                <Thumbnail square small
                                    source={require("../../img/asset/ic-lamp.png")}
                                />
                                <Body>
                                    <Text style={{ fontSize: responsiveFontSize(2) }}>Tuliskan dengan detail alasan Anda me-return laporan.</Text>

                                </Body>
                            </Left>
                        </CardItem>
                    </Card>
                    <Textarea bordered placeholder="Alasan Return" style={{ flex: 1, justifyContent: 'center', alignItems: 'center',  marginTop: "5%", marginRight: "5%", marginLeft: "5%", width: responsiveWidth(90), height: responsiveHeight(40) }} onChangeText={this.handleKomentar} />
                    <Button primary style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: "10%", marginRight: "30%", marginLeft: "30%", width: responsiveWidth(40) }}
                        onPress={this.alasan} >
                        <Text style={{}}>Kirim</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
    handleKomentar = (text) => {
        this.setState({ komentar: text })
    }

    alasan = () => {
        if (this.state.komentar == null || this.state.komentar == "") {
            Alert.alert(
                'Perhatian',
                'Kolom Alasan harus diisi untuk melanjutkan',
                [
                    //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                    //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    { text: 'OK', style: 'cancel' },
                ],
                { cancelable: false }
            )
        } else {

            return fetch("http://ec2-13-250-62-76.ap-southeast-1.compute.amazonaws.com:7000/api/v1/laporan/returnLaporan/" + this.state.nomor_laporan, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    komentar:this.state.komentar
                })
            })
                .then(response => response.json())
                .then((data) => {
                    console.log("Pushed to " + this.state.nomor_laporan + " with data as " + this.state.komentar);
                    this.props.navigation.navigate("ThankYou");
                })
                
        }
    }
}